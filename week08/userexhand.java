import java.util.*;  
class UserDefinedException{  
    public static void main(String args[]){  
        try{  
            throw new NewException(9);  
        }  
        catch(NewException ex){  
            System.out.println(ex) ;  
        }
        finally
        {
            System.out.println("\nFinished");
        }
    }  
}  
class NewException extends Exception{  
    int x;  
    NewException(int y) {  
        x=y;  
    }  
    public String toString(){  
        return ("Exception value =  "+x) ;  
    }  
}  
