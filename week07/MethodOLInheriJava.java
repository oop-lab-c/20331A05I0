class MethodOLInheriJava {
    public void method(int a, int b) {
        System.out.println(a * b);
    }
}

class derived extends MethodOLInheriJava {
    public void method(double a, double b) {
        System.out.println(a + b);
    }

    public static void main(String args[]) {
        derived obj = new derived();
        obj.method(1, 5);
        obj.method(3.9, 4.3);
    }
}
